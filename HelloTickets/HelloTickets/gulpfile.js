﻿"use strict";

var gulp = require("gulp"),
    rimraf = require("rimraf"),
    sass = require("gulp-sass"); 

var paths = {
    webroot: "./wwwroot/assets/"
};

gulp.task("sass", function () {
    return gulp.src('sass/main.scss')
        .pipe(sass())
        .pipe(gulp.dest(paths.webroot + '/css'))
});