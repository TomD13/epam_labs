﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Models.DataControl
{
    public class VenueViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "NameValue", ResourceType = typeof(Resources.Anotation))]
        [Remote(action: "VerifyVenue", controller: "Validation")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Address", ResourceType = typeof(Resources.Anotation))]
        public string Adres { get; set; }

        [Required]
        [Display(Name = "City", ResourceType = typeof(Resources.Anotation))]
        public int CityId { get; set; }

        public IEnumerable<SelectListItem> Cities { get; set; }
    }
}
