﻿using HelloTickets.Models.DataTemplate;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Models.DataControl
{
    public class CityViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "NameValue", ResourceType = typeof(Resources.Anotation))]
        [Remote(action: "VerifyCity", controller: "Validation")]
        public string Name { get; set; }
    }
}
