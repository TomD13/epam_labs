﻿using HelloTickets.Models.DataTemplate;
using HelloTickets.Models.Tags;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Models.DataControl
{
    public class DataListViewModel
    {
        public IEnumerable<City> Cities { get; set; }
        public PageViewModel PageCitiesViewModel { get; set; }

        public IEnumerable<Venue> Venues { get; set; }
        public PageViewModel PageVenueViewModel { get; set; }

        public IEnumerable<Event> Events { get; set; }
        public PageViewModel PageEventViewModel { get; set; }
    }
}
