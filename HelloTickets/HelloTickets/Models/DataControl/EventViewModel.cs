﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Models.DataControl
{
    public class EventViewModel
    {
        public int Id { get; set; }
        [Required]
        [Remote(action: "VerifyEvent", controller: "Validation")]
        [Display(Name = "NameValue", ResourceType = typeof(Resources.Anotation))]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Date", ResourceType = typeof(Resources.Anotation))]
        public DateTime Date { get; set; }
        [Required]
        [Display(Name = "Banner", ResourceType = typeof(Resources.Anotation))]
        public string Banner { get; set; }
        [Required]
        [Display(Name = "Description", ResourceType = typeof(Resources.Anotation))]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Venue", ResourceType = typeof(Resources.Anotation))]
        public int VenueId { get; set; }

        public IEnumerable<SelectListItem> Venues { get; set; }
    }
}
