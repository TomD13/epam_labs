﻿using HelloTickets.Models.DataTemplate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Models.Home
{
    public class TicketsViewModel : Ticket
    {
        public TicketsViewModel() { }

        public TicketsViewModel(Ticket main)
        {
            base.EventId = main.EventId;
            base.Event = main.Event;
            base.Id = main.Id;
            base.Price = main.Price;
            base.SellerId = main.SellerId;
            base.Seller = main.Seller;
        }

        public bool IsValid { get; set; }
    }
}
