﻿using HelloTickets.Models.DataTemplate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Models.Home
{
    public class MyTicketsViewModel
    {
        public IEnumerable<Ticket> Selling { get; set; }
        public IEnumerable<TicketOrderViewModel> WaitingConfirmation  { get; set; }
        public IEnumerable<TicketOrderViewModel> Sold { get; set; }
    }
}
