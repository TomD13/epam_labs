﻿using HelloTickets.Models.DataTemplate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Models.Home
{
    public class TicketOrderViewModel:Ticket
    {
        public TicketOrderViewModel(Ticket main)
        {
            base.Event = main.Event;
            base.EventId = main.EventId;
            base.Id = main.Id;
            base.Price = main.Price;
            base.Seller = main.Seller;
            base.SellerId = main.SellerId;
        }

        public int OrderId { get; set; }
    }
}
