﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Models.Home
{
    public class ATicketsViewModel
    {
        public IEnumerable<TicketsViewModel> Tickets { get; set; }

        public bool confirmation { get; set; }
    }
}
