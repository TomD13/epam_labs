﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Models.Home
{
    public class UserInfoViewModel
    {

        public string Id { get; set; }
        [Required(ErrorMessageResourceName = "FirstNameRequired", ErrorMessageResourceType = typeof(Resources.Anotation))]
        [Display(Name = "FirstName", ResourceType = typeof(Resources.Anotation))]
        public string FirstName { get; set; }
        [Display(Name = "LastName", ResourceType = typeof(Resources.Anotation))]
        public string LastName { get; set; }
        [EmailAddress]
        [Required(ErrorMessageResourceName = "EmailRequired", ErrorMessageResourceType = typeof(Resources.Anotation))]
        [Display(Name = "Email", ResourceType = typeof(Resources.Anotation))]
        public string Email { get; set; }
        [Required(ErrorMessageResourceName = "LocalizationRequired", ErrorMessageResourceType = typeof(Resources.Anotation))]
        [Display(Name = "Localization", ResourceType = typeof(Resources.Anotation))]
        public string Localization { get; set; }
        [Required(ErrorMessageResourceName = "AddressRequired", ErrorMessageResourceType = typeof(Resources.Anotation))]
        [Display(Name = "Address", ResourceType = typeof(Resources.Anotation))]
        public string Adress { get; set; }
        [Required(ErrorMessageResourceName = "PhoneNumberRequired", ErrorMessageResourceType = typeof(Resources.Anotation))]
        [Display(Name = "PhoneNumber", ResourceType = typeof(Resources.Anotation))]
        public string PhoneNomber { get; set; }
    }
}
