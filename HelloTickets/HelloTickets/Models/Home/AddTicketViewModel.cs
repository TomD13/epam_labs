﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Models.Home
{
    public class AddTicketViewModel
    {
        public int Id { get; set; }

        //[Required(ErrorMessageResourceName = "PriceRequired", ErrorMessageResourceType = typeof(Resources.Anotation))]
        [Display(Name = "Price", ResourceType = typeof(Resources.Anotation))]
        public decimal Price { get; set; }

        [Display(Name = "Event", ResourceType = typeof(Resources.Anotation))]
        public int EventId { get; set; }

        public string SellerId { get; set; }

        public IEnumerable<SelectListItem> Events { get; set; }
    }
}
