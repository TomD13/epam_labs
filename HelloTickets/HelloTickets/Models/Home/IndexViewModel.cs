﻿using HelloTickets.Models.DataTemplate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Models.Home
{
    public class IndexViewModel
    {
        public IEnumerable<Event> TopEvents { get; set; }
        public IEnumerable<Ticket> TopTickets { get; set; }
    }
}
