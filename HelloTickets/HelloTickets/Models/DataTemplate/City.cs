﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Models.DataTemplate
{
    public class City
    {
        #region Fields
        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsDelete { get; set; }
        #endregion
    }
}
