﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Models.DataTemplate
{
    public class Event
    {
        #region Fields
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Banner { get; set; }
        public string Description { get; set; }

        public bool IsDelete { get; set; }

        public int VenueId { get; set; }
        public Venue Venue { get; set; }
        #endregion
    }
}
