﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Models.DataTemplate
{
    public class Venue
    {
        #region Fields
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public bool IsDelete { get; set; }

        public int CityId { get; set; }
        public City City { get; set; }
        #endregion
    }
}
