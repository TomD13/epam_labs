﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Models.DataTemplate
{
    public class Ticket
    {
        #region Fields
        public int Id { get; set; }
        public decimal Price { get; set; }
        public bool IsDelete { get; set; }

        public int EventId { get; set; }
        public Event Event { get; set; }

        public string SellerId { get; set; } 
        public User Seller { get; set; }
        #endregion
    }
}
