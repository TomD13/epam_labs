﻿using HelloTickets.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Models.DataTemplate
{
    public class Order
    {
        #region Fields
        public int Id { get; set; }
        public OrderStatus Status { get; set; }
        public bool TrackNO { get; set; }
        public bool IsDelete { get; set; }

        public int TicketId { get; set; }
        public Ticket Ticket { get; set; }

        public string BuyerId { get; set; }
        public User Buyer { get; set; }
        #endregion
    }
}
