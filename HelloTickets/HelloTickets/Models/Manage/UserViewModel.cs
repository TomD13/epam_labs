﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Models.Manage
{
    public class UserViewModel
    {
        public string Id { get; set; }
        [Display(Name = "Name", ResourceType = typeof(Resources.Anotation))]
        public string Name { get; set; }
        [Display(Name = "Email", ResourceType = typeof(Resources.Anotation))]
        public string Email { get; set; }
        [Display(Name = "Role", ResourceType = typeof(Resources.Anotation))]
        public string RoleId { get; set; }

        public IEnumerable<SelectListItem> Roles { get; set; }
    }
}
