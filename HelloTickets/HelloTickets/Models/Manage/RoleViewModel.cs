﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Models.Manage
{
    public class RoleViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
