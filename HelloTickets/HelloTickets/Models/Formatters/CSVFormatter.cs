﻿using Microsoft.AspNetCore.Mvc.Formatters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using Microsoft.Net.Http.Headers;
using System.Reflection;
using System.Globalization;
using Microsoft.AspNetCore.Http;

namespace HelloTickets.Models.Formatters
{
    public class CSVFormatter : TextOutputFormatter
    {
        public CSVFormatter()
        {
            SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/csv"));
            SupportedEncodings.Add(Encoding.ASCII);
            SupportedEncodings.Add(Encoding.Unicode);
            SupportedEncodings.Add(Encoding.UTF8);
        }

        public override Task WriteResponseBodyAsync(OutputFormatterWriteContext context, Encoding selectedEncoding)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            if (selectedEncoding == null)
            {
                throw new ArgumentNullException(nameof(selectedEncoding));
            }

            var response = context.HttpContext.Response;

            if (context.Object is IEnumerable<object>)
                return response.WriteAsync(CSVIEnumerable(CultureInfo.CurrentCulture.TextInfo.ListSeparator, context.Object as IEnumerable<object>));
            else
                return response.WriteAsync(CSVObject(CultureInfo.CurrentCulture.TextInfo.ListSeparator, context.Object));
        }

        #region help
        public string CSVIEnumerable(string sep, IEnumerable<object> list)
        {
            StringBuilder buf = new StringBuilder();

            string head = "";

            list.ElementAt(0).GetType().GetProperties().ToList().ForEach(p => head += p.Name + sep);
            buf.AppendLine(head);

            foreach (var o in list as IEnumerable<object>)
            {
                string temp = "";
                o.GetType().GetProperties().ToList().ForEach(p => temp += p.GetValue(o)?.ToString() + sep);
                buf.AppendLine(temp);
            }
            return buf.ToString();
        }

        public string CSVObject(string sep, object obj)
        {
            StringBuilder buf = new StringBuilder();

            string head = "";

            obj.GetType().GetProperties().ToList().ForEach(p => head += p.Name + sep);
            buf.AppendLine(head);
            string temp = "";
            obj.GetType().GetProperties().ToList().ForEach(p => temp += p.GetValue(obj)?.ToString());
            buf.AppendLine(temp);
            return buf.ToString();
        }
        #endregion
    }
}
