﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Models.Account
{
    public class LoginViewModel
    {
        [Required(ErrorMessageResourceName = "EmailRequired", ErrorMessageResourceType = typeof(Resources.Anotation))]
        [Display(Name = "LoginMail", ResourceType = typeof(Resources.Anotation))]
        public string Login { get; set; }


        [Required(ErrorMessageResourceName = "PasswordRequired", ErrorMessageResourceType = typeof(Resources.Anotation))]
        [Display(Name = "Password", ResourceType = typeof(Resources.Anotation))]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "RememberMe", ResourceType = typeof(Resources.Anotation))]
        public bool RememberMe { get; set; }
    }
}
