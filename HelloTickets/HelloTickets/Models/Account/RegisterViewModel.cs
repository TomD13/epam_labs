﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Models.Account
{
    public class RegisterViewModel
    {
        #region system information
        [EmailAddress]
        [Required(ErrorMessageResourceName = "EmailRequired", ErrorMessageResourceType = typeof(Resources.Anotation))]
        [Display(Name = "Email", ResourceType = typeof(Resources.Anotation))]
        public string Email { get; set; }

        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Required(ErrorMessageResourceName = "PasswordRequired", ErrorMessageResourceType = typeof(Resources.Anotation))]
        [Display(Name = "Password", ResourceType = typeof(Resources.Anotation))]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessageResourceName = "ConfirmPasswordRequired", ErrorMessageResourceType = typeof(Resources.Anotation))]
        [Display(Name = "ConfirmPassword", ResourceType = typeof(Resources.Anotation))]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; } 
        #endregion


        #region user information
        [Required(ErrorMessageResourceName = "FirstNameRequired", ErrorMessageResourceType = typeof(Resources.Anotation))]
        [Display(Name = "FirstName", ResourceType = typeof(Resources.Anotation))]
        public string FirstName { get; set; }

        [Required(ErrorMessageResourceName = "LastNameRequired", ErrorMessageResourceType = typeof(Resources.Anotation))]
        [Display(Name = "LastName", ResourceType = typeof(Resources.Anotation))]
        public string LastName { get; set; }

        [Required(ErrorMessageResourceName = "LocalizationRequired", ErrorMessageResourceType = typeof(Resources.Anotation))]
        [Display(Name = "Localization", ResourceType = typeof(Resources.Anotation))]
        public string Localization
        {
            get;
            set;
        }

        [Required(ErrorMessageResourceName = "AddressRequired", ErrorMessageResourceType = typeof(Resources.Anotation))]
        [Display(Name = "Address", ResourceType = typeof(Resources.Anotation))]
        public string Address { get; set; }

        [Phone]
        [Required(ErrorMessageResourceName = "PhoneNumberRequired", ErrorMessageResourceType = typeof(Resources.Anotation))]
        [Display(Name = "PhoneNumber", ResourceType = typeof(Resources.Anotation))]
        public string PhoneNumber { get; set; }
        #endregion
    }
}
