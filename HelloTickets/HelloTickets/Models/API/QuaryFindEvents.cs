﻿using HelloTickets.Data;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Models.API
{
    public class QuaryFindEvents
    {
        #region Private
        private int pn = 0;
        private int ps = 1;
        #endregion

        #region Public
        [FromRoute]
        public string namepart { get; set; }

        [ModelBinder]
        public int[] cities { get; set; }

        [FromQuery(Name = "datefrom")]
        public DateTime DateFrom { get; set; }

        [FromQuery(Name = "dateto")]
        public DateTime? DateTo { get; set; }

        [FromQuery(Name = "venue")]
        public int VenueId { get; set; }

        [FromQuery(Name = "sorting")]
        public TypeSorting Sorting { get; set; }

        [FromQuery(Name = "pn")]
        public int pageNumber
        {
            get { return pn; }
            set { pn = value; }
        }

        [FromQuery(Name = "ps")]
        public int pageSize
        {
            get { return ps; }
            set { ps = value; }
        } 
        #endregion
    }
}
