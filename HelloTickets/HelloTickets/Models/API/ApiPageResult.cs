﻿using HelloTickets.Models.DataTemplate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Models.API
{
    public class ApiPageResult
    {
        public int Count;
        public int Page;
        public IEnumerable<Event> Result;
    }
}
