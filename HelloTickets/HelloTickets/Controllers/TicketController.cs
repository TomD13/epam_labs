using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HelloTickets.Data;
using HelloTickets.Models.Home;
using HelloTickets.Models.DataTemplate;
using System.Security.Claims;

namespace HelloTickets.Controllers
{
    public class TicketController : Controller
    {
        private TicketDataLayer _tdata;
        private EventDataLayer _edata;

        public TicketController(TicketDataLayer tdata, EventDataLayer edata)
        {
            _tdata = tdata;
            _edata = edata;
        }

        #region Ticket
        [HttpGet]
        public IActionResult AddTicket()
        {
            AddTicketViewModel model = new AddTicketViewModel();
            model.Events = _edata.GetEventsListItem();
            return View(model);
        }

        [HttpPost]
        public IActionResult AddTicket(AddTicketViewModel model)
        {
            if (ModelState.IsValid)
            {
                string userId = User.Claims.First((x) => x.Type == ClaimTypes.NameIdentifier).Value;
                _tdata.AddTicket(new Ticket { EventId = model.EventId, Price = model.Price, SellerId = userId });
                return RedirectToAction("Tickets", "Home");
            }
            else
            {
                model.Events = _edata.GetEventsListItem();
                return View(model);
            }
        }
        #endregion
    }
}