using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HelloTickets.Data;
using System.Security.Claims;

namespace HelloTickets.Controllers
{
    public class TradeController : Controller
    {
        private OrderDataLayer _odata;

        public TradeController(OrderDataLayer odata)
        {
            _odata = odata;
        }

        #region buy
        public async Task<IActionResult> Buy(int ticketID)
        {
            string userId = User.Claims.First((x) => x.Type == ClaimTypes.NameIdentifier).Value;
            bool res = await _odata.AddOrderByTicketId(ticketID, userId);
            return RedirectToAction("Tickets", "Home", new { conf = true });
        }

        public async Task<IActionResult> Confirmation(bool status, int Id)
        {
            if (status)
                await _odata.UpdateStatusOrder(Id, OrderStatus.Confirmed);
            else
                await _odata.UpdateStatusOrder(Id, OrderStatus.Rejected);

            var location = new Uri($"{Request.Scheme}://{Request.Host}/Home/MyTickets#two");
            return Redirect(location.ToString());
        }
        #endregion
    }
}