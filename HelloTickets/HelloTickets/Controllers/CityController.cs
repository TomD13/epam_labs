using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HelloTickets.Data;
using HelloTickets.Models.DataControl;

namespace HelloTickets.Controllers
{
    public class CityController : Controller
    {
        private CityDataLayer _cdata;

        public CityController(CityDataLayer cdata)
        {
            _cdata = cdata;
        }

        #region City
        [HttpGet]
        public IActionResult AddCity()
        {
            return View(new CityViewModel());
        }

        [HttpPost]
        public IActionResult AddCity(CityViewModel model)
        {
            if (ModelState.IsValid)
            {
                _cdata.AddCity(model.Name);
                return RedirectToAction("DataList", "DataControl");
            }
            else
                return View(model);
        }

        [HttpGet]
        public IActionResult EditCity(int id)
        {
            CityViewModel model = new CityViewModel();
            var a = _cdata.GetCityByID(id);
            model.Name = a.Name;
            model.Id = a.Id;
            return View(model);
        }

        [HttpPost]
        public IActionResult EditCity(int id, CityViewModel model)
        {
            _cdata.UpdateCity(id, model.Name);
            return RedirectToAction("DataList", "DataControl");
        }

        public IActionResult RemoveCity(int id)
        {
            _cdata.RemoveCity(id);
            return RedirectToAction("DataList", "DataControl");
        }
        #endregion
    }
}