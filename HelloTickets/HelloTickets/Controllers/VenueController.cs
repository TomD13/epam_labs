using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HelloTickets.Data;
using HelloTickets.Models.DataControl;

namespace HelloTickets.Controllers
{
    public class VenueController : Controller
    {
        private VenueDataLayer _vdata;
        private CityDataLayer _cdata;

        public VenueController(VenueDataLayer vdata, CityDataLayer cdata)
        {
            _vdata = vdata;
            _cdata = cdata;
        }

        #region Venue
        [HttpGet]
        public IActionResult AddVenue()
        {
            VenueViewModel model = new VenueViewModel();
            model.Cities = _cdata.GetCitiesListItem();
            return View(model);
        }

        [HttpPost]
        public IActionResult AddVenue(VenueViewModel model)
        {
            _vdata.AddVenue(model.Name, model.Adres, model.CityId);
            var location = new Uri($"{Request.Scheme}://{Request.Host}/DataControl/DataList#two");
            return Redirect(location.ToString());
        }

        [HttpGet]
        public IActionResult EditVenue(int id)
        {
            var res = _vdata.GetVenueById(id, true);
            VenueViewModel model = new VenueViewModel();
            model.Adres = res.Address;
            model.CityId = res.CityId;
            model.Name = res.Name;
            model.Cities = _cdata.GetCitiesListItem();
            return View(model);
        }

        [HttpPost]
        public IActionResult EditVenue(int id, VenueViewModel model)
        {
            _vdata.UpdateVenue(id, model.Name, model.Adres, model.CityId);
            var location = new Uri($"{Request.Scheme}://{Request.Host}/DataControl/DataList#two");
            return Redirect(location.ToString());
        }

        public IActionResult RemoveVenue(int id)
        {
            _vdata.RemoveVenue(id);
            //придумать как передовать специальные аргументы
            var location = new Uri($"{Request.Scheme}://{Request.Host}/DataControl/DataList#two");
            return Redirect(location.ToString());
        }
        #endregion
    }
}