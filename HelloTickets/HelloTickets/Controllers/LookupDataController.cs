using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using HelloTickets.Data;
using HelloTickets.Models.DataTemplate;

namespace HelloTickets.Controllers
{
    [Produces("application/json")]
    [Route("api/LookupData")]
    public class LookupDataController : Controller
    {
        private CityDataLayer _cdata;
        private VenueDataLayer _vdata;

        public LookupDataController(CityDataLayer cdata, VenueDataLayer vdata)
        {
            _cdata = cdata;
            _vdata = vdata;
        }

        [HttpGet("citiesall")]
        public async Task<IEnumerable<City>> GetCities()
        {
            return await Task.FromResult(_cdata.GetCities());
        }

        [HttpGet("venueall")]
        public async Task<IEnumerable<Venue>> GetVenue()
        {
            return await Task.FromResult(_vdata.GetVenue(false));
        }

        [HttpGet("venuecities")]
        public IEnumerable<Venue> GetVenueContainsCities([FromQuery(Name = "cities")]int[] cities)
        {
            return _vdata.GetVenueContainsCities(cities);
        }

        [HttpGet("citybyid/{id}")]
        public async Task<City> GetCityById(int id)
        {
            return await Task.FromResult(_cdata.GetCityByID(id));
        }

        [HttpGet("venuebyid/{id}")]
        public async Task<Venue> GetVenueById(int id)
        {
            return await Task.FromResult(_vdata.GetVenueById(id, false));
        }
    }
}