using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HelloTickets.Data;
using HelloTickets.Models.DataControl;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;

namespace HelloTickets.Controllers
{
    public class EventController : Controller
    {
        private EventDataLayer _edata;
        private VenueDataLayer _vdata;
        private IHostingEnvironment AppEnvironment;

        public EventController(EventDataLayer edata, 
            VenueDataLayer vdata,
            IHostingEnvironment appEnvironment)
        {
            _edata = edata;
            _vdata = vdata;
            AppEnvironment = appEnvironment;
        }

        #region Event
        [HttpGet]
        public IActionResult AddEvent()
        {
            EventViewModel model = new EventViewModel();
            model.Date = DateTime.Now;
            model.Venues = _vdata.GetVenueListItem();
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddEvent(IFormFile uFile, EventViewModel model)
        {
            if (uFile == null)
            {
                return View(model);
            }

            string path = "/images/" + uFile.FileName;
            using (var fileStream = new FileStream(AppEnvironment.WebRootPath + path, FileMode.Create))
            {
                await uFile.CopyToAsync(fileStream);
            }
            model.Banner = path;
            _edata.AddEvent(model.Name, model.Date, model.Banner, model.Description, model.VenueId);
            var location = new Uri($"{Request.Scheme}://{Request.Host}/DataControl/DataList#three");
            return Redirect(location.ToString());
        }

        [HttpGet]
        public IActionResult EditEvent(int id)
        {
            EventViewModel model = new EventViewModel();
            var res = _edata.GetEventById(id, true);
            model.Name = res.Name;
            model.Banner = res.Banner;
            model.Date = res.Date;
            model.Description = res.Description;
            model.Id = res.Id;
            model.VenueId = res.VenueId;
            model.Venues = _vdata.GetVenueListItem();
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> EditEvent(int id, IFormFile uFile, string LastBanner, EventViewModel model)
        {
            if (uFile != null)
            {
                string path = "/images/" + uFile.FileName;
                using (var fileStream = new FileStream(AppEnvironment.WebRootPath + path, FileMode.Create))
                {
                    await uFile.CopyToAsync(fileStream);
                }
                model.Banner = path;
            }
            else
            {
                model.Banner = LastBanner;
            }

            _edata.UpdateEvent(id, model.Name, model.Date, model.Banner, model.Description, model.VenueId);
            var location = new Uri($"{Request.Scheme}://{Request.Host}/DataControl/DataList#three");
            return Redirect(location.ToString());
        }

        public IActionResult RemoveEvent(int id)
        {
            _edata.RemoveEvent(id);
            var location = new Uri($"{Request.Scheme}://{Request.Host}/DataControl/DataList#three");
            return Redirect(location.ToString());
        }
        #endregion
    }
}