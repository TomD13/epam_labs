using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using HelloTickets.Models.DataControl;
using HelloTickets.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using HelloTickets.Models.Home;
using HelloTickets.Models.DataTemplate;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using HelloTickets.Models.Tags;

namespace HelloTickets.Controllers
{
    [Authorize]
    public class DataControlController : Controller
    {
        private CityDataLayer _cdata;
        private VenueDataLayer _vdata;
        private EventDataLayer _edata;

        private IHostingEnvironment AppEnvironment;

        public DataControlController(IHostingEnvironment appEnvironment,
            CityDataLayer cdata,
            VenueDataLayer vdata,
            EventDataLayer edata)
        {
            _cdata = cdata;
            _vdata = vdata;
            _edata = edata;
            AppEnvironment = appEnvironment;
        }

        public IActionResult DataList(int pageC = 1, int pageV = 1, int pageE = 1)
        {
            int pageSize = 5;
            DataListViewModel model = new DataListViewModel();

            var cities = _cdata.GetCities();
            var resC = cities.ToList().Skip((pageC - 1) * pageSize).Take(pageSize);
            PageViewModel pageCitiesViewModel = new PageViewModel(cities.Count(), pageC, pageSize);
            model.Cities = resC;
            model.PageCitiesViewModel = pageCitiesViewModel;

            var venue = _vdata.GetVenue(true);
            var resV = venue.ToList().Skip((pageV - 1) * pageSize).Take(pageSize);
            PageViewModel pageVenueViewModel = new PageViewModel(venue.Count(), pageV, pageSize);
            model.Venues = resV;
            model.PageVenueViewModel = pageVenueViewModel;

            var events = _edata.GetEvents(true);
            var resE = events.ToList().Skip((pageE - 1) * pageSize).Take(pageSize);
            PageViewModel pageEventViewModel = new PageViewModel(events.Count(), pageE, pageSize);
            model.Events = resE;
            model.PageEventViewModel = pageEventViewModel;
            return View(model);
        }
    }
}