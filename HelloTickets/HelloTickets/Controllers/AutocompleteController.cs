using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using HelloTickets.Data;
using HelloTickets.Models.DataTemplate;

namespace HelloTickets.Controllers
{
    [Produces("application/json")]
    [Route("api/Autocomplete")]
    public class AutocompleteController : Controller
    {
        EventDataLayer Context;

        public AutocompleteController(EventDataLayer context)
        {
            Context = context;
        }

        [HttpGet("acevent/{name}")]
        public async Task<IEnumerable<Event>> AutocompleteEventsName(string name)
        {
            return await Context.GetEventNameContainsValueAsync(name);
        }
    }
}