﻿using HelloTickets.Data;
using HelloTickets.Models.DataTemplate;
using HelloTickets.Models.Home;
using HelloTickets.Models.Manage;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Controllers
{
    [Authorize]
    public class UserController: Controller
    {
        private UserDataLayer _udata;
        private RoleDataLayer _rdata;
        private readonly SignInManager<User> SignInManager;

        public UserController(
            UserDataLayer udata,
            RoleDataLayer rdata,
            SignInManager<User> signInManager)
        {
            _udata = udata;
            _rdata = rdata;
            SignInManager = signInManager;
        }

        [HttpGet]
        public IActionResult Users()
        {
            List<UserListViewModel> model = new List<UserListViewModel>();
            var us = _udata.GetUsers();
            model = us.Select(u => new UserListViewModel
                                        {
                                            Id = u.Id,
                                            Name = u.UserName,
                                            Email = u.Email,
                                            RoleName = _rdata.GetFirstRoleNameByUser(u)
                                        }).Where((X) => X.RoleName != "super").ToList();
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> EditUser(string idUser)
        {
            User u = await _udata.GetUserByIdAsync(idUser);
            UserViewModel model = new UserViewModel()
                                        {
                                            Email = u.Email,
                                            Id = idUser,
                                            Name = u.UserName,
                                            RoleId = _rdata.GetRoleIdToUser(u),
                                            Roles = _rdata.GetAllRoles()
                                        };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditUser(string idUser, UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.Id = idUser;
                var result = _udata.UpdateUserAsync(model);
                if(result.Result.Succeeded)
                    return RedirectToAction("Users");
            }

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> EditCurrentUser()
        {
            User u = await _udata.GetUserByNameAsync(User.Identity.Name);
            UserInfoViewModel model = new UserInfoViewModel();
            model.Id = u.Id;
            model.FirstName = u.FirstName;
            model.Email = u.Email;
            model.Localization = u.Localization;
            model.LastName = u.LastName;
            model.Adress = u.Address;
            model.PhoneNomber = u.PhoneNumber;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditCurrentUser(string id, UserInfoViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.Id = id;
                var result = _udata.UpdateCurrentUserAsync(model);
                if (result.Result.Succeeded)
                {
                    User u = await _udata.GetUserByIdAsync(model.Id);
                    await SignInManager.RefreshSignInAsync(u);
                    Response.Cookies.Append(
                        CookieRequestCultureProvider.DefaultCookieName,
                        CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(model.Localization)),
                        new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
);
                    return RedirectToAction(nameof(HomeController.UserInfo), "Home");
                }

                return View(model);
            }

            return View(model);
        }
    }
}
