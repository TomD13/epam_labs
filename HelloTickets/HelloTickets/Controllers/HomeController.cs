using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Http;
using HelloTickets.Models.DataTemplate;
using HelloTickets.Models.Home;
using HelloTickets.Models;
using HelloTickets.Data;
using System.Security.Claims;

namespace HelloTickets.Controllers
{
    public class HomeController : Controller
    {
        private CityDataLayer _cdata;
        private VenueDataLayer _vdata;
        private EventDataLayer _edata;
        private TicketDataLayer _tdata;
        private OrderDataLayer _odata;
        private UserDataLayer _udata;

        public HomeController(
            CityDataLayer cdata, 
            VenueDataLayer vdata, 
            EventDataLayer edata, 
            TicketDataLayer tdata,
            OrderDataLayer odata,
            UserDataLayer udata)
        {
            _cdata = cdata;
            _vdata = vdata;
            _edata = edata;
            _tdata = tdata;
            _odata = odata;
            _udata = udata;
        }

        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return LocalRedirect(returnUrl);
        }

        public async Task<IActionResult> Index()
        {
            string uname = User.Identity.Name;
            IndexViewModel model = new IndexViewModel();
            model.TopEvents = await _edata.GetEventsTakeAsync(3);

            model.TopTickets = await _tdata.GetTicketsTakeAsync(3);
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Events()
        {
            IEnumerable<Event> events = await _edata.GetAllEventsAsync();
            return View(events);
        }

        public async Task<IActionResult> Tickets(int ID, bool conf)
        {
            ATicketsViewModel model = new ATicketsViewModel();
            List<TicketsViewModel> tickets = new List<TicketsViewModel>(1);
            if (ID != 0)
            {
                var res = await _tdata.GetTicketsByEventAsync(ID);
                foreach (Ticket t in res)
                {
                    TicketsViewModel tvm = new TicketsViewModel(t);
                    tvm.IsValid = await _odata.ExistTicketAndBuyerFromOrder(t.Id, t.SellerId);
                    tickets.Add(tvm);
                };
                var resName = await _edata.GetEventByIdAsync(ID);
                ViewData["NameEvents"] = resName.Name;
            }
            else
            {
                var res = await _tdata.GetAllTicketsAsync();
                foreach (Ticket t in res)
                {
                    TicketsViewModel tvm = new TicketsViewModel(t);
                    tvm.IsValid = await _odata.ExistTicketAndBuyerFromOrder(t.Id, t.SellerId);
                    tickets.Add(tvm);
                };
            }
            model.confirmation = conf;
            model.Tickets = tickets;
            return View(model);
        }

        [Authorize]
        public async Task<IActionResult> MyTickets()
        {
            string userId = User.Claims.First((x) => x.Type == ClaimTypes.NameIdentifier).Value;
            MyTicketsViewModel model = new MyTicketsViewModel();
            model.Selling = await _tdata.GetTicketsByUserAsync(userId);

            var res = await _odata.GetStatusTicketsByUserAsync(userId, OrderStatus.WaitingForConfirmation);
            model.WaitingConfirmation = Convert(res);

            var res1 = await _odata.GetStatusTicketsByUserAsync(userId, OrderStatus.Confirmed);
            model.Sold = Convert(res1);
            return View(model);
        }

        [Authorize]
        public async Task<IActionResult> MyOrders()
        {
            IEnumerable<Order> model = await _odata.GetOrderByUserAsync(User.Claims.First((x) => x.Type == ClaimTypes.NameIdentifier).Value);
            return View(model);
        }

        [Authorize]
        public async Task<IActionResult> UserInfo()
        {
            User u = await _udata.GetUserByIdAsync(User.Claims.First((x) => x.Type == ClaimTypes.NameIdentifier).Value);
            UserInfoViewModel model = new UserInfoViewModel();
            model.FirstName = u.FirstName;
            model.LastName = u.LastName;
            model.Localization = u.Localization;
            model.Email = u.Email;
            model.Adress = u.Address;
            model.PhoneNomber = u.PhoneNumber;
            return View(model);
        }


        private IEnumerable<TicketOrderViewModel> Convert(IEnumerable<Order> main)
        {
            string userId = User.Claims.First((x) => x.Type == ClaimTypes.NameIdentifier).Value;
            List<TicketOrderViewModel> list = new List<TicketOrderViewModel>();
            foreach(Order o in main.ToList())
            {
                TicketOrderViewModel item = new TicketOrderViewModel(o.Ticket);
                item.OrderId = o.Id;
                list.Add(item);
            }
            return list;
        }
    }
}