using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using HelloTickets.Data;
using HelloTickets.Models.DataTemplate;
using HelloTickets.Models.API;

namespace HelloTickets.Controllers
{
    [Produces("application/json")]
    [Route("api/Finder")]
    public class FinderApiController : Controller
    {
        EventDataLayer _edata;

        public FinderApiController(EventDataLayer edata)
        {
            _edata = edata;
        }

        [HttpGet("eventsall")]
        public IEnumerable<Event> GetAllEvents()
        {
            return _edata.GetEvents(false);
        }

        [HttpGet("find/{namepart}")]
        public IActionResult FindEvents(
            [FromRoute]string namepart,
            [ModelBinder] int[] cities,
            [FromQuery(Name = "datefrom")] DateTime DateFrom,
            [FromQuery(Name = "dateto")] DateTime? DateTo,
            [FromQuery(Name = "venue")] int VenueId,
            [FromQuery(Name = "sorting")] TypeSorting Sorting,
            [FromQuery(Name = "pn")] int pageNumber = 0,
            [FromQuery(Name = "ps")] int pageSize = 1)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            int count = 0;
            IEnumerable<Event> @event = _edata.GetEvents(true);
            //name
            if (namepart != null)
            {
                @event = @event.Where(m => m.Name.ToUpper().Contains(namepart.ToUpper())).ToList();
            }

            //date
            if (DateTo != null)
            {
                @event = @event.Where((x) => x.Date > DateFrom && x.Date < DateTo).ToList();
            }
            else
            {
                @event = @event.Where((x) => x.Date > DateFrom).ToList();
            }

            //cities
            if (cities != null && cities.Length > 0)
            {
                @event = @event.Where((x) => cities.Contains(x.Venue.CityId)).ToList();
            }

            if (VenueId != -1)
            {
                @event = @event.Where((x) => x.VenueId == VenueId).ToList();
            }

            //sorting
            switch (Sorting)
            {
                case TypeSorting.Name:
                    @event.ToList().Sort(delegate (Event x, Event y)
                    { return x.Name.CompareTo(y.Name); }); break;

                case TypeSorting.Date:
                    @event.ToList().Sort(delegate (Event x, Event y)
                    { return x.Date.CompareTo(y.Date); }); break;
            }

            //venue
            if (@event == null)
            {
                return NotFound();
            }

            count = @event.Count();
            var ur = $"{this.Request.Scheme}://{this.Request.Host}";
            return Ok(new ApiPageResult()
            {
                Result = @event.Skip(pageNumber * pageSize).ToList().Take(pageSize).Select(x => new Event()
                {
                    Id = x.Id,
                    Banner = x.Banner.Replace("~/", ur + "/"),
                    Date = x.Date,
                    Description = x.Description,
                    Name = x.Name,
                    VenueId = x.VenueId
                }),
                Page = pageNumber,
                Count = count
            });
        }

        [HttpGet("find2/{namepart}")]
        public IActionResult FindEvents2(QuaryFindEvents quary)
        {
            return FindEvents(quary.namepart, 
                              quary.cities, 
                              quary.DateFrom, 
                              quary.DateTo, 
                              quary.VenueId, 
                              quary.Sorting, 
                              quary.pageNumber, 
                              quary.pageSize);
        }

        [HttpGet("eventbyid/{id}")]
        public Event GetEvent(int id)
        {
            return _edata.GetEventById(id, false);
        }
    }
}