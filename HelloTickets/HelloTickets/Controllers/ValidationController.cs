using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using HelloTickets.Data;

namespace HelloTickets.Controllers
{
    public class ValidationController : Controller
    {
        private CityDataLayer _cdata;
        private VenueDataLayer _vdata;
        private EventDataLayer _edata;

        public ValidationController(CityDataLayer cdata, VenueDataLayer vdata, EventDataLayer edata)
        {
            _cdata = cdata;
            _vdata = vdata;
            _edata = edata;
        }

        [AcceptVerbs("Get", "Post")]
        public IActionResult VerifyCity(string Name)
        {
            if (_cdata.ExistCityName(Name))
                return Json("City name exist in base");
            else
                return Json(true);
        }

        [AcceptVerbs("Get", "Post")]
        public IActionResult VerifyVenue(string Name)
        {
            if (_vdata.ExistVenueName(Name))
                return Json("Venue name exist in base");
            else
                return Json(true);
        }

        [AcceptVerbs("Get", "Post")]
        public IActionResult VerifyEvent(string Name)
        {
            if (_edata.ExistEventName(Name))
                return Json("Event name exist in base");
            else
                return Json(true);
        }
    }
}