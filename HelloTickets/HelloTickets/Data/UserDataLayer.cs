﻿using HelloTickets.Models.DataTemplate;
using HelloTickets.Models.Home;
using HelloTickets.Models.Manage;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Data
{
    public class UserDataLayer
    {
        UserManager<User> UManager;
        RoleManager<IdentityRole> RManager;

        public UserDataLayer(UserManager<User> usersm, RoleManager<IdentityRole> rolesm)
        {
            UManager = usersm;
            RManager = rolesm;
        }

        #region User
        public async Task<IdentityResult> AddUserAsync(User NewUser, string password)
        {
            return await UManager.CreateAsync(NewUser, password);
        }

        public User[] GetUsers()
        {
            User[] users = null;
            users = UManager.Users.ToArray();
            return users;
        }

        public async Task<User> GetUserByIdAsync(string id)
        {
            User u = await UManager.FindByIdAsync(id);
            return u;
        }

        public async Task<User> GetUserByNameAsync(string UName)
        {
            User u = await UManager.FindByNameAsync(UName);
            return u;
        }

        public async Task<IdentityResult> UpdateUserAsync(UserViewModel model)
        {
            User user = await GetUserByIdAsync(model.Id);
            if (user != null)
            {
                user.UserName = model.Name;
                user.Email = model.Email;

                string existingRole = UManager.GetRolesAsync(user).Result.Single();
                string existingRoleId = RManager.Roles.Single(r => r.Name == existingRole).Id;

                IdentityResult result = await UManager.UpdateAsync(user);

                if (result.Succeeded)
                {
                    if (existingRoleId != model.RoleId)
                    {
                        IdentityResult roleResult = await UManager.RemoveFromRoleAsync(user, existingRole);
                        if (roleResult.Succeeded)
                        {
                            IdentityRole applicationRole = await RManager.FindByIdAsync(model.RoleId);
                            if (applicationRole != null)
                            {
                                IdentityResult newRoleResult = await UManager.AddToRoleAsync(user, applicationRole.Name);
                                if (newRoleResult.Succeeded)
                                {
                                    return await Task<IdentityResult>.Run(() => { return IdentityResult.Success; });
                                }
                            }
                        }
                    }
                }
            }
            return await Task<IdentityResult>.Run(() => { return IdentityResult.Failed(); });
        }

        public async Task<IdentityResult> UpdateCurrentUserAsync(UserInfoViewModel model)
        {
            User user = await GetUserByIdAsync(model.Id);
            if (user != null)
            {
                user.FirstName = model.FirstName;
                user.LastName = model.LastName != null ? model.LastName : "";
                user.Email = user.Email;

                user.UserName = model.LastName != "" && model.LastName != null ? model.FirstName + "_" + model.LastName : model.FirstName;

                user.Localization = model.Localization;

                user.Address = model.Adress;
                user.PhoneNumber = model.PhoneNomber;

                IdentityResult result = await UManager.UpdateAsync(user);
                if (result.Succeeded)
                {
                    //signin
                    return await Task<IdentityResult>.Run(() => { return IdentityResult.Success; });
                }
            }
            return await Task<IdentityResult>.Run(() => { return IdentityResult.Failed(); });
        }

        public void initializeData()
        {
            if (!UManager.Users.Any())
            {
                #region Simple user
                User NewSimpleUser = new User()
                {
                    NormalizedUserName = "USER",
                    UserName = "User",
                    NormalizedEmail = "USER@MAIL.COM",
                    Email = "user@mail.com",
                    FirstName = "User",
                    LastName = "",
                    Localization = "be",
                    Address = "Gomel, Soveckay 21",
                    PhoneNumber = "80333333333"
                };

                IdentityResult result = UManager.CreateAsync(NewSimpleUser, "user").Result;
                if (result.Succeeded)
                {
                    IdentityRole applicationRole = RManager.FindByNameAsync("user").Result;
                    if (applicationRole != null)
                    {
                        IdentityResult roleResult = UManager.AddToRoleAsync(NewSimpleUser, "user").Result;
                    }
                }
                #endregion

                #region Admin
                User NewAdmin = new User
                {
                    NormalizedUserName = "ADMIN",
                    UserName = "Admin",
                    NormalizedEmail = "ADMIN@MAIL.COM",
                    Email = "admin@mail.com",
                    FirstName = "Admin",
                    LastName = "",
                    Localization = "ru",
                    Address = "Gomel, Soveckay 22",
                    PhoneNumber = "80222222222",
                };

                IdentityResult result1 = AddUserAsync(NewAdmin, "admin").Result;
                if (result.Succeeded)
                {
                    IdentityRole applicationRole = RManager.FindByNameAsync("sadmin").Result;
                    if (applicationRole != null)
                    {
                        IdentityResult roleResult = UManager.AddToRoleAsync(NewAdmin, "sadmin").Result;
                    }
                }
                #endregion
            }
        }
        #endregion
    }
}
