﻿using HelloTickets.Models.DataTemplate;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Data
{
    public class VenueDataLayer
    {
        HelloTicketsDbContext Context;

        public VenueDataLayer(HelloTicketsDbContext context)
        {
            Context = context;
        }

        #region Venue
        public IEnumerable<Venue> GetVenue(bool entry = true)
        {
            var list = Context.Venues.Where((x) => !x.IsDelete).ToList();
            if (entry)
            {
                foreach (var i in list)
                {
                    Context.Entry(i).Reference("City").Load();
                }
            }
            return list;
        }

        public bool ExistVenueName(string Name)
        {
            return Context.Venues.ToList().Where((x) => x.Name == Name).Count() != 0;
        }

        public Venue GetVenueById(int id, bool entry = true)
        {
            Venue res = Context.Venues.Where((x) => !x.IsDelete).FirstOrDefault((x) => x.Id == id);
            if (entry)
                Context.Entry(res).Reference("City").Load();
            return res;
        }

        public IEnumerable<Venue> GetVenueContainsCities(IEnumerable<int> cities)
        {
            return Context.Venues.Where(x => cities.Contains(x.CityId)).ToList();
        }

        public void AddVenue(string Name, string Adres, int CityId)
        {
            Context.Venues.Add(new Venue() { Name = Name, Address = Adres, CityId = CityId });
            Context.SaveChanges();
        }

        public void UpdateVenue(int id, string Name, string Adres, int CityId)
        {
            var res = Context.Venues.FirstOrDefault((x) => x.Id == id);
            if (res.Name != Name
                || res.Address != Adres
                    || res.CityId != CityId)
            {
                res.Name = Name;
                res.Address = Adres;
                res.CityId = CityId;
                Context.Venues.Update(res);
                Context.SaveChanges();
            }
        }

        public void RemoveVenue(int id)
        {
            var res = Context.Venues.FirstOrDefault((x) => x.Id == id);
            res.IsDelete = true;
            Context.Venues.Update(res);
            Context.SaveChanges();
        }

        public IEnumerable<SelectListItem> GetVenueListItem()
        {
            return Context.Venues.Where((x) => !x.IsDelete).Select(r => new SelectListItem
            {
                Text = r.Name,
                Value = r.Id.ToString()
            }).ToList();
        }

        public bool ExistVenueInEvent(Venue venue)
        {
            return Context.Events.Where((x) => x.VenueId == venue.Id).Count() == 0;
        }

        public void initializeData()
        {
            if (!Context.Venues.Any())
            {
                City[] cities = Context.Cities.ToArray();
                Context.Venues.AddRange(
                    new Venue() { CityId = cities[0].Id, Name = "Concert hall \"Center\"", Address = "1092 Godfrey Road", IsDelete = false },
                    new Venue() { CityId = cities[0].Id, Name = "Concert hall \"2\"", Address = "4024 Farnum Road", IsDelete = false },
                    new Venue() { CityId = cities[1].Id, Name = "Large concert hall", Address = "1811 C Street", IsDelete = false },
                    new Venue() { CityId = cities[1].Id, Name = "Smal concert hall", Address = "2897 Cody Ridge Road", IsDelete = false },
                    new Venue() { CityId = cities[2].Id, Name = "Large concert hall", Address = "1007 Horizon Circle", IsDelete = false },
                    new Venue() { CityId = cities[2].Id, Name = "Large concert hall", Address = "2214 Levy Court", IsDelete = false },
                    new Venue() { CityId = cities[3].Id, Name = "Smal concert hall", Address = "2711 Counts Lane", IsDelete = false },
                    new Venue() { CityId = cities[3].Id, Name = "Smal concert hall", Address = "352 Hall Place", IsDelete = false },
                    new Venue() { CityId = cities[4].Id, Name = "Smal concert hall", Address = "2846 Christie Way", IsDelete = false },
                    new Venue() { CityId = cities[4].Id, Name = "Large concert hall", Address = "1709 Modoc Alley", IsDelete = false },
                    new Venue() { CityId = cities[5].Id, Name = "Concert hall \"Minsk\"", Address = "Oktyabrskaya street 5", IsDelete = false },
                    new Venue() { CityId = cities[5].Id, Name = "Prime Hall", Address = "Ave. Winners 65", IsDelete = false }
                    );
                Context.SaveChanges();
            }
        }
        #endregion
    }
}
