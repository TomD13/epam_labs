﻿using HelloTickets.Models.DataTemplate;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Data
{
    public class EventDataLayer
    {
        HelloTicketsDbContext Context;

        public EventDataLayer(HelloTicketsDbContext context)
        {
            Context = context;
        }

        #region Event
        public IEnumerable<Event> GetEvents(bool entry = true)
        {
            var list = Context.Events.Where((x) => !x.IsDelete).ToList();
            if (entry)
            {
                foreach (var i in list)
                {
                    Context.Entry(i).Reference("Venue").Load();
                    Context.Entry(i.Venue).Reference("City").Load();
                }
            }
            return list;
        }

        public async Task<IEnumerable<Event>> GetEventNameContainsValueAsync(string value)
        {
            return await Task.FromResult(Context.Events.Where(x => x.Name.Contains(value)).ToList());
        }

        public bool ExistEventName(string Name)
        {
            return Context.Events.ToList().Where((x) => x.Name == Name).Count() != 0;
        }

        public Event GetEventById(int id, bool entry = true)
        {
            var res = Context.Events.Where((x) => !x.IsDelete).FirstOrDefault((x) => x.Id == id);
            if (entry)
            {
                Context.Entry(res).Reference("Venue").Load();
                Context.Entry(res.Venue).Reference("City").Load();
            }
            return res;
        }

        public void AddEvent(string Name, DateTime Date, string Banner, string Description, int VenueId)
        {
            Context.Events.Add(new Event
            {
                Name = Name,
                Date = Date,
                Banner = Banner,
                Description = Description,
                VenueId = VenueId
            });
            Context.SaveChanges();
        }

        public void UpdateEvent(int id, string Name, DateTime Date, string Banner, string Description, int VenueId)
        {
            var res = Context.Events.FirstOrDefault((x) => x.Id == id);
            if (res.Name != Name ||
                res.Date != Date ||
                    res.Banner != Banner ||
                        res.Description != Description ||
                            res.VenueId != VenueId)
            {
                res.Name = Name;
                res.Date = Date;
                res.Banner = Banner;
                res.Description = Description;
                res.VenueId = VenueId;
                Context.Events.Update(res);
                Context.SaveChanges();
            }
        }

        public void RemoveEvent(int id)
        {
            var res = Context.Events.FirstOrDefault((x) => x.Id == id);
            res.IsDelete = true;
            Context.SaveChanges();
        }

        public async Task<string> GetNameEventsByIdAsync(int id)
        {
            return await Task<string>.Run(() =>
            {
                string name = Context.Events.Where((x) => !x.IsDelete).FirstOrDefault((x) => x.Id == id).Name;
                return name;
            });
        }

        public async Task<List<Event>> GetAllEventsAsync()
        {
            return await Task<IEnumerable<Event>>.Run(() =>
            {
                var list = Context.Events.Where((x) => !x.IsDelete).ToList();
                foreach (var i in list)
                {
                    Context.Entry(i).Reference("Venue").Load();
                    Context.Entry(i.Venue).Reference("City").Load();
                }
                return list;
            });
        }

        public async Task<List<Event>> GetEventsTakeAsync(int count)
        {
            return await Task<IEnumerable<Event>>.Run(() =>
            {
                var list = Context.Events.Where((x) => !x.IsDelete).Take(count).ToList();
                foreach (var i in list)
                {
                    Context.Entry(i).Reference("Venue").Load();
                    Context.Entry(i.Venue).Reference("City").Load();
                }
                return list;
            });
        }

        public async Task<Event> GetEventByIdAsync(int id)
        {
            return await Task<Event>.Run(() =>
            {
                var a = Context.Events.Where((x) => !x.IsDelete).FirstOrDefault((x) => x.Id == id);
                if (a != null)
                {
                    Context.Entry(a).Reference("Venue").Load();
                    Context.Entry(a.Venue).Reference("City").Load();
                }
                return a;
            });
        }

        public IEnumerable<SelectListItem> GetEventsListItem()
        {
            return Context.Events.Where((x) => !x.IsDelete).Select(r => new SelectListItem
            {
                Text = r.Name,
                Value = r.Id.ToString()
            }).ToList();
        }

        public bool ExistEventInTicket(Event ev)
        {
            return Context.Tickets.Where((x) => x.EventId == ev.Id).Count() == 0;
        }

        public void initializeData()
        {
            if (!Context.Events.Any())
            {
                Venue[] venues = Context.Venues.ToArray();
                Context.Events.AddRange(
                    new Event() { VenueId = venues[0].Id, Name = "Concert 1", Date = DateTime.Now.AddDays(5), Banner = "~/images/pic004.png", IsDelete = false },
                    new Event() { VenueId = venues[1].Id, Name = "Concert 2", Date = DateTime.Now.AddDays(-5), Banner = "~/images/pic003.png", IsDelete = false },
                    new Event() { VenueId = venues[2].Id, Name = "Concert 3", Date = DateTime.Now.AddDays(15), Banner = "~/images/pic002.png", IsDelete = false },
                    new Event() { VenueId = venues[3].Id, Name = "Concert 4", Date = DateTime.Now.AddDays(-15), Banner = "~/images/pic001.png", IsDelete = false },
                    new Event() { VenueId = venues[4].Id, Name = "Concert 5", Date = DateTime.Now.AddYears(1), Banner = "~/images/pic006.png", IsDelete = false }
                    );

                Context.SaveChanges();
            }
        }
        #endregion
    }
}
