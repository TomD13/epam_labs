﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Data
{ 
    public enum OrderStatus
    {
        WaitingForConfirmation,
        Confirmed,
        Rejected
    }

    public enum TypeSorting : int
    {
        Name = 1,
        Date = 2
    }
}
