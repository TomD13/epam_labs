﻿using HelloTickets.Models.DataTemplate;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Data
{
    public class CityDataLayer
    {
        HelloTicketsDbContext Context;

        public CityDataLayer(HelloTicketsDbContext context)
        {
            Context = context;
        }

        #region City
        public IEnumerable<City> GetCities()
        {
            return Context.Cities.Where((x) => !x.IsDelete).ToList();
        }

        public bool ExistCityName(string Name)
        {
            return Context.Cities.ToList().Where((x) => x.Name == Name).Count() != 0;
        }

        public City GetCityByID(int id)
        {
            return Context.Cities.Where((x) => !x.IsDelete).FirstOrDefault((x) => x.Id == id);
        }

        public void AddCity(string Name)
        {
            Context.Cities.Add(new City() { Name = Name });
            Context.SaveChanges();
        }

        public void UpdateCity(int id, string NewName)
        {
            var res = Context.Cities.FirstOrDefault((x) => x.Id == id);
            if (res.Name != NewName)
            {
                res.Name = NewName;
                Context.Cities.Update(res);
                Context.SaveChanges();
            }
        }

        public void RemoveCity(int id)
        {
            var res = Context.Cities.FirstOrDefault((x) => x.Id == id);
            res.IsDelete = true;
            Context.Cities.Update(res);
            Context.SaveChanges();
        }

        public IEnumerable<SelectListItem> GetCitiesListItem()
        {
            return Context.Cities.Where((x) => !x.IsDelete).Select(r => new SelectListItem
            {
                Text = r.Name,
                Value = r.Id.ToString()
            }).ToList();
        }

        public bool ExistCityInVenue(City city)
        {
            return Context.Venues.Where((x) => x.CityId == city.Id).Count() == 0;
        }

        public void initializeData()
        {
            if (!Context.Cities.Any())
            {
                Context.Cities.AddRange(
                    new City() { Name = "New York", IsDelete = false },
                    new City() { Name = "Boston", IsDelete = false },
                    new City() { Name = "Seattle", IsDelete = false },
                    new City() { Name = "Paris", IsDelete = false },
                    new City() { Name = "Moscow", IsDelete = false },
                    new City() { Name = "Minsk", IsDelete = false }
                    );
                Context.SaveChanges();
            }
        }
        #endregion
    }
}
