﻿using HelloTickets.Models.DataTemplate;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Data
{
    public class HelloTicketsDbContext: IdentityDbContext<User>
    {
        #region DBSet
        public DbSet<City> Cities { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Venue> Venues { get; set; }
        #endregion

        public HelloTicketsDbContext(DbContextOptions<HelloTicketsDbContext> options) : base(options) { }
    }
}
