﻿using HelloTickets.Models.DataTemplate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Data
{
    public class TicketDataLayer
    {
        HelloTicketsDbContext Context;

        public TicketDataLayer(HelloTicketsDbContext context)
        {
            Context = context;
        }

        #region Ticket
        public void AddTicket(Ticket ticket)
        {
            Context.Tickets.Add(ticket);
            Context.SaveChanges();
        }

        public async Task<List<Ticket>> GetAllTicketsAsync()
        {
            return await Task<List<Ticket>>.Run(() =>
            {
                var list = Context.Tickets.Where((x) => !x.IsDelete).ToList();
                foreach (var j in list)
                {
                    Context.Entry(j).Reference("Event").Load();
                    Context.Entry(j).Reference("Seller").Load();
                    Context.Entry(j.Event).Reference("Venue").Load();
                    Context.Entry(j.Event.Venue).Reference("City").Load();
                }
                return list;
            });
        }

        public async Task<List<Ticket>> GetTicketsTakeAsync(int count)
        {
            return await Task<List<Ticket>>.Run(() =>
            {
                var list = Context.Tickets.Where((x) => !x.IsDelete).Take(count).ToList();
                foreach (var j in list)
                {
                    Context.Entry(j).Reference("Event").Load();
                    Context.Entry(j).Reference("Seller").Load();
                    Context.Entry(j.Event).Reference("Venue").Load();
                    Context.Entry(j.Event.Venue).Reference("City").Load();
                }
                return list;
            });
        }

        public async Task<List<Ticket>> GetTicketsByEventAsync(int EventId)
        {
            return await Task<List<Ticket>>.Run(() =>
            {
                var list = Context.Tickets.Where((x) => x.EventId == EventId && !x.IsDelete).ToList();
                foreach (var j in list)
                {
                    Context.Entry(j).Reference("Event").Load();
                    Context.Entry(j).Reference("Seller").Load();
                    Context.Entry(j.Event).Reference("Venue").Load();
                    Context.Entry(j.Event.Venue).Reference("City").Load();
                }
                return list;
            });
        }

        public async Task<List<Ticket>> GetTicketsByUserAsync(string UserId)
        {
            return await Task<List<Ticket>>.Run(() =>
            {
                var list = Context.Tickets.Where((x) => x.SellerId == UserId && !x.IsDelete).ToList();
                foreach (var j in list)
                {
                    Context.Entry(j).Reference("Event").Load();
                    Context.Entry(j).Reference("Seller").Load();
                    Context.Entry(j.Event).Reference("Venue").Load();
                    Context.Entry(j.Event.Venue).Reference("City").Load();
                }
                return list;
            });
        }

        public int GetOrderIdByTicketUser(int TicketId, string UserId)
        {
            var orderticket = Context.Orders.ToList().FirstOrDefault((x) => x.TicketId == TicketId && x.Ticket.SellerId == UserId && !x.IsDelete);

            if (orderticket != null)
                return orderticket.Id;
            else
                return -1;
        }

        public void initializeData()
        {
            if (!Context.Tickets.Any())
            {
                Event[] events = Context.Events.ToArray();
                User[] users = Context.Users.ToArray();
                Context.Tickets.AddRange(
                    new Ticket() { EventId = events[0].Id, Price = 10.5m, SellerId = users[1].Id, IsDelete = false },
                    new Ticket() { EventId = events[0].Id, Price = 24.51m, SellerId = users[1].Id, IsDelete = false },
                    new Ticket() { EventId = events[1].Id, Price = 34.4m, SellerId = users[1].Id, IsDelete = false },
                    new Ticket() { EventId = events[2].Id, Price = 11.12m, SellerId = users[1].Id, IsDelete = false },
                    new Ticket() { EventId = events[3].Id, Price = 24.78m, SellerId = users[1].Id, IsDelete = false },
                    new Ticket() { EventId = events[4].Id, Price = 46.2m, SellerId = users[1].Id, IsDelete = false }
                    );
                Context.SaveChanges();
            }
        }
        #endregion
    }
}
