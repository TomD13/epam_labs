﻿using HelloTickets.Models.DataTemplate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Data
{
    public class OrderDataLayer
    {
        HelloTicketsDbContext Context;

        public OrderDataLayer(HelloTicketsDbContext context)
        {
            Context = context;
        }

        #region Order
        public async Task<List<Order>> GetOrderByUserAsync(string UserId)
        {
            return await Task<List<Order>>.Run(() =>
            {
                var list = Context.Orders.Where((X) => X.BuyerId == UserId && !X.IsDelete).ToList();
                foreach (var j in list)
                {
                    Context.Entry(j).Reference("Ticket").Load();
                    Context.Entry(j.Ticket).Reference("Event").Load();
                    Context.Entry(j.Ticket).Reference("Seller").Load();
                    Context.Entry(j.Ticket.Event).Reference("Venue").Load();
                    Context.Entry(j.Ticket.Event.Venue).Reference("City").Load();

                    Context.Entry(j).Reference("Buyer").Load();
                }
                return list;
            });
        }

        public async Task<bool> AddOrderByTicketId(int TicketId, string BayerId)
        {
            return await Task<bool>.Run(() =>
            {
                var tic = Context.Tickets.FirstOrDefault((x) => x.Id == TicketId && !x.IsDelete);
                if (tic == null)
                    return false;

                var buyer = Context.Users.FirstOrDefault((x) => x.Id == BayerId);
                if (buyer == null)
                    return false;

                Order order = new Order
                {
                    BuyerId = BayerId,
                    Status = OrderStatus.WaitingForConfirmation,
                    TicketId = TicketId,
                    TrackNO = true
                };
                Context.Orders.Add(order);
                Context.SaveChanges();
                return true;
            });
        }

        public async Task<bool> ExistTicketAndBuyerFromOrder(int TicketId, string BuyerId)
        {
            return await Task<bool>.Run(() =>
            {
                var res = Context.Orders.Where((x) => x.BuyerId == BuyerId && x.TicketId == TicketId && !x.IsDelete);
                if (res.Count() == 0)
                    return true;
                else
                    return false;
            });
        }

        public async Task<bool> UpdateStatusOrder(int OrderId, OrderStatus status)
        {
            return await Task<bool>.Run(() =>
            {
                var res = Context.Orders.FirstOrDefault((x) => x.Id == OrderId && !x.IsDelete);

                if (res == null)
                    return false;

                res.Status = status;
                Context.Orders.Update(res);
                Context.SaveChanges();

                return true;
            });
        }

        public async Task<List<Order>> GetStatusTicketsByUserAsync(string UserId, OrderStatus status)
        {
            return await Task<List<Order>>.Run(() =>
            {
                var list = Context.Tickets.Where((x) => x.SellerId == UserId && !x.IsDelete).ToList();
                if (list.Count == 0)
                    return new List<Order>(1);

                var orderTicketsList = Context.Orders.Where((x) => list.Where((y) => y.Id == x.TicketId).Count() > 0 && x.Status == status).ToList();
                foreach (var j in orderTicketsList)
                {
                    Context.Entry(j).Reference("Ticket").Load();
                    Context.Entry(j.Ticket).Reference("Event").Load();
                    Context.Entry(j.Ticket).Reference("Seller").Load();
                    Context.Entry(j.Ticket.Event).Reference("Venue").Load();
                    Context.Entry(j.Ticket.Event.Venue).Reference("City").Load();

                    Context.Entry(j).Reference("Buyer").Load();
                }
                return orderTicketsList;
            });
        }

        public void initializeData()
        {
            if (!Context.Orders.Any())
            {
                Ticket[] tickets = Context.Tickets.ToArray();
                User[] users = Context.Users.ToArray();
                Context.Orders.AddRange(
                    new Order() { TicketId = tickets[1].Id, Status = OrderStatus.Confirmed, BuyerId = users[0].Id, TrackNO = false, IsDelete = false },
                    new Order() { TicketId = tickets[2].Id, Status = OrderStatus.Confirmed, BuyerId = users[0].Id, TrackNO = false, IsDelete = false },
                    new Order() { TicketId = tickets[3].Id, Status = OrderStatus.WaitingForConfirmation, BuyerId = users[0].Id, TrackNO = false, IsDelete = false },
                    new Order() { TicketId = tickets[4].Id, Status = OrderStatus.Rejected, BuyerId = users[0].Id, TrackNO = false, IsDelete = false }
                    );
                Context.SaveChanges();
            }
        }
        #endregion
    }
}
