﻿using HelloTickets.Models.DataTemplate;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloTickets.Data
{
    public class RoleDataLayer
    {
        RoleManager<IdentityRole> RManager;
        UserManager<User> UManager;

        public RoleDataLayer(UserManager<User> usersm, RoleManager<IdentityRole> rolesm)
        {
            UManager = usersm;
            RManager = rolesm;
        }

        #region Role
        public IEnumerable<SelectListItem> GetAllRoles()
        {
            return RManager.Roles.Where((X) => X.Name != "sadmin").Select(r => new SelectListItem
            {
                Text = r.Name,
                Value = r.Id
            }).ToList();
        }

        public string GetFirstRoleNameByUser(User u)
        {
            return RManager.Roles.Single(r => r.Name == UManager.GetRolesAsync(u).Result.Single()).Name;
        }

        public string GetRoleIdToUser(User u)
        {
            return RManager.Roles.Single(r => r.Name == UManager.GetRolesAsync(u).Result.Single()).Id;
        }

        public void initializeData()
        {
            #region Roles
            if (!RManager.Roles.Any())
            {
                IdentityRole roleuser = new IdentityRole("user") { NormalizedName = "USER" };
                RManager.CreateAsync(roleuser).Wait();

                IdentityRole roleadmin = new IdentityRole("admin") { NormalizedName = "ADMIN" };
                RManager.CreateAsync(roleadmin).Wait();

                IdentityRole rolesuper = new IdentityRole("sadmin") { NormalizedName = "SADMIN" };
                RManager.CreateAsync(rolesuper).Wait();

                IdentityRole rolemanager = new IdentityRole("manager") { NormalizedName = "MANAGER" };
                RManager.CreateAsync(rolemanager).Wait();
            }
            #endregion
        }
        #endregion
    }
}
