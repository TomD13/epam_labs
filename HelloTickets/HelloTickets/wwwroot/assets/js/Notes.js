﻿//options
//-cssText
//-className
//-Header
//-Content
//-time
    function shownote(options) {
          var parent = document.getElementById("NotifyPanel"); 
          var notification = document.createElement('li');
          notification.className = "nitem";
          if (options.cssText) {
                notification.style.cssText = options.cssText;
          }
          if (options.className) {
                notification.classList.add(options.className);
          }

          notification.innerHTML = options.html;

          parent.appendChild(notification);

          setTimeout(function() {
            parent.removeChild(notification);
          }, (options.time || 1500));
    }