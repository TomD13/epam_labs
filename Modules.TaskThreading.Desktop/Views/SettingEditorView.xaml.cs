﻿using Modules.TaskThreading.Desktop.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Modules.TaskThreading.Desktop.Views
{
    /// <summary>
    /// Логика взаимодействия для SettingEditorView.xaml
    /// </summary>
    public partial class SettingEditorView : UserControl
    {
        public SettingEditorView(SettingEditorViewModel view)
        {
            InitializeComponent();

            DataContext = view;
        }
    }
}
