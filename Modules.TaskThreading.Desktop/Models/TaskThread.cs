﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modules.TaskThreading.Desktop.Models
{
    public class TaskThread : INotifyPropertyChanged
    {
        #region Private
        private string URL;
        private long TimeOut;
        #endregion

        public TaskThread(string url, long timeOut)
        {
            URL = url;
            TimeOut = timeOut;
        }

        #region Public
        public void Run()
        {

        }
        #endregion

        #region Notify
        public event PropertyChangedEventHandler PropertyChanged;

        private void Change(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
