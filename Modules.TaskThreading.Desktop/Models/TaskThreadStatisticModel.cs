﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modules.TaskThreading.Desktop.Models
{
    public class TaskThreadStatisticModel
    {
        #region Private
        private TaskThreadingSetting _Settings { get; set; }

        private int _TotalRequests { get; set; }

        private double _BandwidthRequests { get; set; }

        private int _TotalErrors { get; set; }

        private long _MinResponse { get; set; }
        private long _MaxResponse { get; set; }
        private long _AverageResponse { get; set; }
        #endregion

        public TaskThreadStatisticModel(TaskThreadingSetting settings, 
                                        int totalRequests, 
                                        double bandwidthRequests,
                                        int totalErrors,
                                        long minResponse,
                                        long maxResponse,
                                        long averageResponse)
        {
            _Settings = settings;
            _TotalRequests = totalRequests;
            _BandwidthRequests = bandwidthRequests;
            _TotalErrors = totalErrors;
            _MinResponse = minResponse;
            _MaxResponse = maxResponse;
            _AverageResponse = averageResponse;
        }

        #region Public
        public string Setting { get { return _Settings.ToString(); } }

        public string TotalRequests { get { return "Total requests: " + _TotalRequests.ToString(); } }

        public string BandwidthRequests { get { return "Bandwidth requests(r/s): " + _BandwidthRequests.ToString(); } }

        public string TotalErrors { get { return "Total errors: " + _TotalErrors.ToString(); } }

        public string MinResponse { get { return "Min response(ms): " + _MinResponse.ToString(); } }

        public string MaxResponse { get { return "Max response(ms): " + _MaxResponse.ToString(); } }

        public string AverageResponse { get { return "Average response(ms): " + _AverageResponse.ToString(); } } 
        #endregion
    }
}
