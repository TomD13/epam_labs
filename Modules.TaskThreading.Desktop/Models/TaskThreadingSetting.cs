﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modules.TaskThreading.Desktop.Models
{
    public class TaskThreadingSetting
    {
        public TaskThreadingSetting(int countThread, int countRequest, string url, int timeOut)
        {
            CountThread = countThread;
            CountRequest = countRequest;
            URL = url;
            TimeOut = timeOut;
        }

        public int CountThread { get; set; }
        public int CountRequest { get; set; }
        public string URL { get; set; }
        public int TimeOut { get; set; }

        public override string ToString()
        {
            return $"Settings: Count thread: {CountThread}, Count request: {CountRequest}, URL: {URL}, TimeOut: {TimeOut}";
        }
    }
}
