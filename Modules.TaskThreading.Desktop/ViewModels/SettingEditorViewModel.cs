﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.PubSubEvents;
using Modules.TaskThreading.Desktop.Models;
using Modules.TaskThreading.Desktop.Modules;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Modules.TaskThreading.Desktop.ViewModels
{
    public class SettingEditorViewModel : INotifyPropertyChanged, IDataErrorInfo
    {
        #region Private
        private readonly IDictionary<string, string> errors = new Dictionary<string, string>();
        private readonly TaskThreadingSetting _settings;
        private int _Current = 0;
        private List<TaskThreadResult> _TotalRequestsPerSecond;
        private List<TaskThreadResult> _SuccessfulRequestsPerSecond;
        private List<TaskThreadResult> _FailedRequestsPerSecond;

        private volatile bool _TaskExecution = false;
        private volatile bool _ActiveExecution = false;
        private volatile bool _Finally = false;
        #endregion

        public SettingEditorViewModel()
        {
            _settings = new TaskThreadingSetting(1, 1, "http://microsoft.com", 100000);

            _TotalRequestsPerSecond = new List<TaskThreadResult>();
            _SuccessfulRequestsPerSecond = new List<TaskThreadResult>();
            _FailedRequestsPerSecond = new List<TaskThreadResult>();

            this.RunCommand = new DelegateCommand<object>(this.Run, this.CanRun);

            this.StopCommand = new DelegateCommand(() => { ActiveExecution = false; });

            this.ReportCommand = new DelegateCommand<object>(this.Report);
        }

        #region Public
        public bool TaskExecution
        {
            get { return _TaskExecution; }
            set { _TaskExecution = value; Change(nameof(TaskExecution)); }
        }

        public bool ActiveExecution
        {
            get { return _ActiveExecution; }
            set { _ActiveExecution = value; Change(nameof(ActiveExecution)); }
        }

        public bool Finally
        {
            get { return _Finally; }
            set { _Finally = value; Change(nameof(Finally)); }
        }

        public TaskThreadingSetting Settings
        {
            get { return _settings; }
        }

        public IEnumerable<TaskThreadResult> TotalRequestsPerSecondAll {
            get {
                return _TotalRequestsPerSecond.ToList();
            }
        }

        public IEnumerable<TaskThreadResult> SuccessfulRequestsPerSecondAll {
            get {
                return _SuccessfulRequestsPerSecond.ToList();
            }
        }

        public IEnumerable<TaskThreadResult> FailedRequestsPerSecondAll {
            get {
                return _FailedRequestsPerSecond.ToList();
            }
        }

        public IEnumerable<TaskThreadResult> TotalRequestsPerSecond10 {
            get {
                int count = _TotalRequestsPerSecond.Count;
                return _TotalRequestsPerSecond.Skip(count >= 10 ? count - 10 : 0).Take(10).ToList();
            }
        }

        public IEnumerable<TaskThreadResult> SuccessfulRequestsPerSecond10 {
            get {
                int count = _SuccessfulRequestsPerSecond.Count;
                return _SuccessfulRequestsPerSecond.Skip(count >= 10 ? count -10 : 0).Take(10).ToList();
            }
        }

        public IEnumerable<TaskThreadResult> FailedRequestsPerSecond10 {
            get {
                int count = _FailedRequestsPerSecond.Count;
                return _FailedRequestsPerSecond.Skip(count >= 10 ? count - 10 :0 ).Take(10).ToList();
            }
        }

        public TaskThreadStatisticModel Statistic { get; set; }

        public int CountRequest { get { return _settings.CountRequest * _settings.CountThread; } }

        public int Current { get { return _Current;} set { _Current = value; Change(nameof(Current)); } }

        public long MaxResponse = 0;
        public long MinResponse = 0;

        public List<KeyValuePair<string, int>> CorrelationOfResults
        {
            get {
                List<KeyValuePair<string, int>> temp = new List<KeyValuePair<string, int>>(3);
                int ctr = 0;
                foreach (var item in _SuccessfulRequestsPerSecond)
                    ctr += item.Result;
                temp.Add(new KeyValuePair<string, int>("Successful requests", ctr));

                ctr = 0;
                foreach (var item in _FailedRequestsPerSecond)
                    ctr += item.Result;
                temp.Add(new KeyValuePair<string, int>("Failed requests", ctr));
                return temp;
            }
        }

        #region Void
        public void AddRequestsResult(IEnumerable<TaskThreadResult> result, 
                                      IEnumerable<TaskThreadResult> successResult,
                                      IEnumerable<TaskThreadResult> failedResult)
        {
            foreach (var item in result)
                _TotalRequestsPerSecond.Add(item);

            foreach (var item in successResult)
                _SuccessfulRequestsPerSecond.Add(item);

            foreach (var item in failedResult)
                _FailedRequestsPerSecond.Add(item);
        }
        #endregion

        #endregion

        #region Run Command

        public event EventHandler<DataEventArgs<SettingEditorViewModel>> Launched;

        public DelegateCommand<object> RunCommand { get; private set; }

        public DelegateCommand StopCommand { get; private set; }

        public DelegateCommand<object> ReportCommand { get; private set; }

        private bool CanRun(object arg)
        {
            return this.errors.Count == 0;
        }

        private void Run(object obj)
        {
            this.OnLaunched(new DataEventArgs<SettingEditorViewModel>(this));
        }

        private void Report(object value)
        {
            SaveFileDialog winDialog = new SaveFileDialog();
            winDialog.Filter = "pdf|*.pdf";
            winDialog.FileName = "Report.pdf";
            switch (winDialog.ShowDialog())
            {
                case DialogResult.OK:
                    {
                        PdfDocument document = new PdfDocument();

                        PdfPage page = document.AddPage();

                        XGraphics gfx = XGraphics.FromPdfPage(page);

                        XFont fontH = new XFont("Times new roman", 20, XFontStyle.Bold);

                        XFont font = new XFont("Times new roman", 14);

                        #region Settings
                        gfx.DrawString("Settings", fontH, XBrushes.Black,
                      10, 10,
                      XStringFormat.TopLeft);
                        gfx.DrawString("Count task : " + _settings.CountThread,
                            font,
                            XBrushes.Black,
                            20, 40,
                            XStringFormat.TopLeft);
                        gfx.DrawString("Count request : " + _settings.CountRequest, font, XBrushes.Black,
                            20, 60,
                            XStringFormat.TopLeft);

                        gfx.DrawString("URl : " + _settings.URL, font, XBrushes.Black,
                            20, 80,
                            XStringFormat.TopLeft);

                        gfx.DrawString("Time out : " + _settings.TimeOut, font, XBrushes.Black,
                            20, 100,
                            XStringFormat.TopLeft);
                        #endregion

                        #region Graphics
                        Grid cnReport = (Grid)value; 
                        XImage img = XImage.FromGdiPlusImage(ImageToPNG(cnReport));
                        gfx.DrawImage(img, 20, 120);
                        #endregion

                        document.Save(winDialog.FileName);

                        Process.Start(winDialog.FileName);
                        break;
                    }
            }
        }

        public System.Drawing.Image ImageToPNG(FrameworkElement frameworkElement)
        {
            Transform transform = frameworkElement.LayoutTransform;
            frameworkElement.LayoutTransform = null;
            Thickness margin = frameworkElement.Margin;
            frameworkElement.Margin = new Thickness(0, 0, margin.Right - margin.Left, margin.Bottom - margin.Top);
            System.Windows.Size size = new System.Windows.Size(frameworkElement.ActualWidth, frameworkElement.ActualHeight);
            frameworkElement.Measure(size);
            frameworkElement.Arrange(new Rect(size));
            RenderTargetBitmap bmp = new RenderTargetBitmap((int)size.Width, (int)size.Height, 96, 96, PixelFormats.Pbgra32);
            bmp.Render(frameworkElement);
            frameworkElement.LayoutTransform = transform;
            frameworkElement.Margin = margin;
            PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Interlace = PngInterlaceOption.On;
            encoder.Frames.Add(BitmapFrame.Create(bmp));
            MemoryStream stream = new MemoryStream();
            encoder.Save(stream);
            return System.Drawing.Image.FromStream(stream);
        }

        private void OnLaunched(DataEventArgs<SettingEditorViewModel> e)
        {
            EventHandler<DataEventArgs<SettingEditorViewModel>> launchedHandler = this.Launched;
            if (launchedHandler != null)
            {
                launchedHandler(this, e);
            }
            Change(nameof(CountRequest));

            DateTime dt = DateTime.Now;
            Current = 0;
            _TotalRequestsPerSecond.Clear();
            _SuccessfulRequestsPerSecond.Clear();
            _FailedRequestsPerSecond.Clear();
            Change(nameof(TotalRequestsPerSecond10));
            Change(nameof(SuccessfulRequestsPerSecond10));
            Change(nameof(FailedRequestsPerSecond10));
            TaskExecution = true;
            ActiveExecution = true;
            Task task = Task.Factory.StartNew(() =>
            {
                Parallel.For(0, e.Value.Settings.CountThread, (a) =>
                {
                    Stopwatch watch;
                    for (int i = 0; i < e.Value.Settings.CountRequest; i++)
                    {
                        if (!ActiveExecution)
                            return;

                        watch = new Stopwatch();
                        watch.Start();
                        using (HttpClient client = new HttpClient())
                        {
                            if (e.Value.Settings.TimeOut != 0)
                                client.Timeout = new TimeSpan(0, 0, 0, 0, e.Value.Settings.TimeOut);
                            if (MinResponse == 0)
                                MinResponse = client.MaxResponseContentBufferSize;
                            ChangeTimeResult((int)(DateTime.Now - dt).TotalSeconds, _TotalRequestsPerSecond, nameof(TotalRequestsPerSecond10));
                            try
                            {
                                using (HttpResponseMessage response = client.GetAsync(e.Value.Settings.URL).Result)
                                {
                                    if (response.IsSuccessStatusCode)
                                        ChangeTimeResult((int)(DateTime.Now - dt).TotalSeconds, _SuccessfulRequestsPerSecond, nameof(SuccessfulRequestsPerSecond10));
                                    else
                                        ChangeTimeResult((int)(DateTime.Now - dt).TotalSeconds, _FailedRequestsPerSecond, nameof(FailedRequestsPerSecond10));
                                }
                            }
                            catch
                            {
                                ChangeTimeResult((int)(DateTime.Now - dt).TotalSeconds, _FailedRequestsPerSecond, nameof(FailedRequestsPerSecond10));
                            }
                        }
                        watch.Stop();
                        if (MaxResponse < watch.ElapsedMilliseconds)
                            MaxResponse = watch.ElapsedMilliseconds;
                        if (MinResponse > watch.ElapsedMilliseconds)
                            MinResponse = watch.ElapsedMilliseconds;
                        Current++;
                    }
                });
                TaskExecution = false;
                ActiveExecution = false;
                Change(nameof(TotalRequestsPerSecondAll));
                Change(nameof(SuccessfulRequestsPerSecondAll));
                Change(nameof(FailedRequestsPerSecondAll));

                Change(nameof(CorrelationOfResults));

                Statistic = new TaskThreadStatisticModel(e.Value.Settings,
                                                        _TotalRequestsPerSecond.Sum(x => x.Result),
                                                        _TotalRequestsPerSecond.Sum(x => x.Result) / (_TotalRequestsPerSecond[_TotalRequestsPerSecond.Count - 1].Second + 1),
                                                        _FailedRequestsPerSecond.Sum(x => x.Result),
                                                        MinResponse,
                                                        MaxResponse,
                                                        (MinResponse + MaxResponse) / 2);
                Change(nameof(Statistic));
            });

            Task MainTask = Task.Factory.StartNew(() =>
            {
                Finally = false;
                while (_TaskExecution)
                {
                    Thread.Sleep(1000);
                    int time = (int)(DateTime.Now - dt).TotalSeconds;
                    while (time >= _TotalRequestsPerSecond.Count())
                    {
                        _TotalRequestsPerSecond.Add(new TaskThreadResult(_TotalRequestsPerSecond.Count(), 0));
                    }
                    Change(nameof(TotalRequestsPerSecond10));
                    while (time >= _SuccessfulRequestsPerSecond.Count())
                    {
                        _SuccessfulRequestsPerSecond.Add(new TaskThreadResult(_SuccessfulRequestsPerSecond.Count(), 0));
                    }
                    Change(nameof(SuccessfulRequestsPerSecond10));
                    while (time >= _FailedRequestsPerSecond.Count())
                    {
                        _FailedRequestsPerSecond.Add(new TaskThreadResult(_FailedRequestsPerSecond.Count(), 0));
                    }
                    Change(nameof(FailedRequestsPerSecond10));
                }
                Finally = true;
            });
        }

        private IEnumerable<TaskThreadResult> ChangeTimeResult(int time, List<TaskThreadResult> list, string propertyName)
        {
            while (time >= list.Count())
            {
                list.Add(new TaskThreadResult(list.Count(), 0));
            }
            list[time].Result++;
            return list;
        }
        #endregion

        #region DataError

        public string this[string columnName]
        {
            get
            {
                if (this.errors.ContainsKey(columnName))
                {
                    return this.errors[columnName];
                }
                return null;
            }
            set
            {
                this.errors[columnName] = value;
            }
        }

        public string Error
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region Notify
        public event PropertyChangedEventHandler PropertyChanged;
        public void Change(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
