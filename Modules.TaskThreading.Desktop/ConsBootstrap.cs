﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modules.TaskThreading.Desktop
{
    public class ConsBootstrap
    {
        Bootstrapper bootstrapper;

        public ConsBootstrap()
        {
            bootstrapper = new Bootstrapper();
        }

        public void Run()
        {
            bootstrapper.Run();
        }
    }
}
