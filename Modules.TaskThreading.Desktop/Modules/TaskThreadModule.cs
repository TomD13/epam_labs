﻿using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Unity;
using Modules.TaskThreading.Desktop.ViewModels;
using Modules.TaskThreading.Desktop.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modules.TaskThreading.Desktop.Modules
{
    public class TaskThreadModule : IModule
    {
        private readonly IRegionManager regionManager;
        private readonly IUnityContainer container;

        public TaskThreadModule(IUnityContainer container, IRegionManager regionManager)
        {
            this.container = container;
            this.regionManager = regionManager;
        }

        public void Initialize()
        {
            this.regionManager.RegisterViewWithRegion("SettingRegion",
                                                       () => this.container.Resolve<SettingEditorView>());
        }
    }
}
