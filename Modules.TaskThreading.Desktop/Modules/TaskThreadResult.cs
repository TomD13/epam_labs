﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modules.TaskThreading.Desktop.Modules
{
    public class TaskThreadResult:INotifyPropertyChanged
    {
        #region Private
        private int _Second;
        private int _Result;
        #endregion

        public TaskThreadResult(int second, int result)
        {
            Second = second;
            Result = result;
        }

        #region Public
        public int Second { get { return _Second; } set { _Second = value; Change(nameof(Second)); } }
        public int Result { get { return _Result; } set { _Result = value; Change(nameof(Result)); } } 
        #endregion

        #region Notify
        public event PropertyChangedEventHandler PropertyChanged;
        public void Change(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
